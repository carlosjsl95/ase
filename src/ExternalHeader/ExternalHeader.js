import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../assets/logo.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faQuestionCircle
} from '@fortawesome/free-regular-svg-icons';
import {
    faAdjust,
    faAssistiveListeningSystems,
} from '@fortawesome/free-solid-svg-icons';
import { HashLink } from 'react-router-hash-link';
import AccessibiltyOptions from '../AccessibiltyOptions/AccessibiltyOptions';

class ExternalHeader extends Component {
    render() {
        return (
            <header className="header">
                <nav className="navbar navbar-light bg-white justify-content-between align-items-end" id="scrollspynav">
                    <a className="navbar-brand" href="/">
                        <img className="logo" src={Logo} alt="EvaEduc" />
                    </a>
                    <div className="navbar navbar-menu">
                        <div className="btn-group mr-3">
                            <HashLink to="/sobre#sobre" className="btn bg-transparent border-right nav-link btn-lg">Sobre</HashLink>
                            <HashLink to="/sobre#equipe" className="btn bg-transparent nav-link border-left btn-lg">Equipe</HashLink>
                        </div>
                        <AccessibiltyOptions/>
                        <Link to="/cadastrar">
                            <button type="button" className="btn bg-white border margin-vertical btn-justify">Cadastrar</button>
                        </Link>
                        <Link to="/login">
                            <button type="button" className="btn btn-primary horizontal-padding btn-justify">Entrar</button>
                        </Link>
                    </div>
                </nav>
            </header>
        )
    }
}
export default ExternalHeader;