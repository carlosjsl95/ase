import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { faAdjust, faAssistiveListeningSystems } from '@fortawesome/free-solid-svg-icons';

class AccessibiltyOptions extends Component {
    setHighContrastMode() {
        document.body.classList.toggle('contrast');
    }

    render() {
        return (
            <div className="btn-group mr-3">
                <button type="button" className="btn bg-white" onClick={this.setHighContrastMode}><FontAwesomeIcon icon={faAdjust} size="lg" /></button>
                <button type="button" className="btn bg-white "><FontAwesomeIcon icon={faAssistiveListeningSystems} size="lg" /></button>
                <button type="button" className="btn bg-white "><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button>
            </div>
        )
    }
}

export default AccessibiltyOptions;