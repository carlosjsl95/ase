import React, { Component } from 'react';
import Logo from '../assets/logo.svg';
import * as loginService from '../services/index';
import './Register.sass';

class Register extends Component {

	state = {
		data: {
			name: null,
			username: null,
			email: null,
			password: null,
		},
		error: null,
	}

	register = async () => {
		const { data } = this.state;
		try {
			await loginService.register(data);
			const response = await loginService.login(data);
			loginService.setCredentials(response.data.token);
			this.props.history.push("/avaliacao")
			window.localStorage.setItem('user', JSON.stringify(data));
		} catch (error) {
			this.setState({
				error: 'Não foi possivel realizar o cadastro'
			})
		}
	}

	handleInputName = (event, inputName) => {
		const { data } = this.state;
    this.setState({
			data: {
				...data,
				[inputName]: event.target.value,
			}
    });
  }

	render() {
		return (
			<div className="register-container">
				<div className="register-box">
					<img src={Logo} className="register-box__logo mb-3" alt="EvaEduc"/>
					<h3 className="register-box__title mb-3">Cadastre-se</h3>
					<div className="register-box__inputs">
						<div className ="mb-3">
							<label className="sr-only" htmlFor="name">Nome Completo</label>
							<input type="text" className="form-control" placeholder="Nome Completo" name="name" onChange={(e) => this.handleInputName(e, 'name')}></input>
						</div>
						<div className ="mb-3">
							<label className="sr-only" htmlFor="user">Usuário</label>
							<input type="text" className="form-control" placeholder="Usuário" name="user" onChange={(e) => this.handleInputName(e, 'username')}></input>
						</div>
						<div className ="mb-3">
							<label className="sr-only" htmlFor="e-mail">E-mail</label>
							<input type="email" className="form-control" placeholder="E-mail" name="e-mail" onChange={(e) => this.handleInputName(e, 'email')}></input>
						</div>
						<div className ="mb-3">
							<label className="sr-only" htmlFor="password">Senha</label>
							<input type="password" className="form-control" placeholder="Senha" name="password" onChange={(e) => this.handleInputName(e, 'password')}></input>
						</div>
						<div className ="mb-3">
							<label className="sr-only" htmlFor="passwordconfirm" >Confirme a senha</label>
							<input type="password" className="form-control" placeholder="Confirme a senha" name="passwordconfirm" onChange={(e) => this.handleInputName(e, 'passwordConfirmation')}></input>
						</div>
					</div>
					<div className="register-box__buttons-container">
							<button type="button" className="btn btn-green border pr-5 pl-5" onClick={this.register}>Salvar</button>
					</div>
				</div>
			</div>
		)
	}
}

export default Register;
