import React, { Component } from 'react';
import Logo from '../assets/logo.svg';
import { Link } from 'react-router-dom';
import * as loginService from '../services/index';
import './Login.sass';

class Login extends Component {

  state = {
    email: null,
    password: null,
    erro: null,
  };

  doLogin = async () => {
    const { email, password } = this.state;

    if (!email || !password) {
      this.setState({
        error: 'Por favor preenchar todos os campos'
      });
      return;
    }

    try {
      const response = await loginService.login({ email, password });
      loginService.setCredentials(response.data.token);
      this.props.history.push('/avaliacao');
    } catch (error) {
      if(error.response.status === 401) {
        this.setState({
          error: error.response.status === 401 ? 'Credenciais inválidas' : 'Ocorreu um erro no sistema'
        });
      }
    }
  }

  handleInputName = (event, inputName) => {
    this.setState({
      [inputName]: event.target.value,
    });
  }

  render() {
    const { error } = this.state;
    return (
      <div className="login-container">
        <div className="login-box">
          <img src={Logo} className="login-box__logo" alt="EvaEduc"/>
          <h3 className="login-box__title">Login</h3>
          <div className="login-box__inputs">
            <input type="email" class="form-control" placeholder="Email" onChange={(event) => this.handleInputName(event, 'email')}></input>
            <input type="password" class="form-control" placeholder="Senha" onChange={(event) => this.handleInputName(event, 'password')}></input>
          </div>
          {error && <p style={{ color: 'red' }}>{error}</p>}
          <a href="#" className="login-box__recover-password-text">Esqueceu sua senha?</a>
          <div className="login-box__buttons-container">
          <Link to="/cadastrar">
            <button type="button" className="btn btn-light login-box__buttons---no-color border">Criar conta</button>
          </Link>
          <button type="button" className="btn bg-white login-box__buttons border btn-green" onClick={this.doLogin}>Entrar</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Login;
