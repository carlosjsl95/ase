import axios from 'axios';

axios.defaults.baseURL = 'https://evaeduc-api.herokuapp.com';

export default axios;