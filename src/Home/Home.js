import React, { Component } from 'react'
import './Home.sass';
import ExternalHeader from '../ExternalHeader/ExternalHeader';

class Home extends Component {
  render() {
    return (
      <div className="container">
        <ExternalHeader />
        <main>
          <div className="container main">
            <h1>Avaliar agora é simples.</h1>
            <p>Conheça um pouco mais da nossa ferramenta de avaliação de softwares</p>
            <p>educativos, com ela avaliar tudo é bem mais fácil. Experimente agora!</p>
            <div className="separator" />
            <p>Não se preocupe! Durante toda a avalição, nós iremos te ajudar a entender</p>
            <p>melhor um pouquinho de cada método. Vamos lá?</p>
            <button type="button" className="btn btn-primary btn-small btn-justify">Começar</button>
          </div>
          <div className="row mt-4">
            <div className="col-12 col-md-8 mb-3 col-sm-12">
              <p className="text-justify" >
                O EvaEduc é uma ferramenta de avaliação de software criada por alunos do curso de Bacharelado
               em Tecnologia da informação, do Instituro Metrópole Digital (IMD), da Universidade Federal
               do Rio Grande do Norte(UFRN), sob orientação da Professora Adja Ferreira de Andrade.
               </p>
               <p className="text-justify">
                A ferramenta vai permitir a análise de software da web, desktop e mobile. As questões foram feitas
                com base em métodos de avaliação já reconhecidos. Sendo algumas questões criadas por nossa equipe
                visando atender a critérios pedagógicos usabilidade ergonômica e acessibilidade.
              </p>
            </div>
            <div className="col-12 col-md-4 mb-3 col-sm-12">
              <div>
                <h3>Necessita de mais infromações para usar a ferramenta? </h3>
              </div>
              <div>
                <button className="btn btn-secondary">Acesse o Manual de Ajuda</button>
              </div>
            </div>
          </div>
        </main>
      </div>
    )
  }
}

export default Home;
