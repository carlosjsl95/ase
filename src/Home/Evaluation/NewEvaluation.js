import React, { Component } from 'react'
import './NewEvaluation.sass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { faAccessibleIcon } from '@fortawesome/free-brands-svg-icons';
import {
  faDesktop,
  faMobileAlt,
  faGlobe,
  faChalkboardTeacher
} from '@fortawesome/free-solid-svg-icons';
import Header from '../../Header/Header';
import * as service from '../../services/index';
import Usability from '../../assets/usability.svg';
import ReactTooltip from 'react-tooltip'

class NewEvaluation extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: {
        name: null,
        platform: null,
        categories: [],
      }
    }

  }

  createEvaluation = async () => {
    const { data } = this.state;
    try {
      await service.newEvaluation(data);
      this.props.history.push('/questionario');
    } catch (error) {
      console.log(error.message)
    }
  }

  setPlatform = (platform) => {
    const { data } = this.state;
    this.setState({
      data: {
        ...data,
        platform,
      }
    });
  }

  setCategory = (category) => {
    const { data } = this.state
    this.setState({
      data: {
        ...data,
        categories: data.categories.includes(category) ?
          data.categories.filter(el => el !== category) : [...data.categories, category],
      }
    })
  }

  render() {
    const { data } = this.state
    return (
      <div className="container">
        <Header />
        <main>
          <div className="container">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb" style={{ backgroundColor: 'rgba(233,236,239, 0.5)' }}>
                <li className="breadcrumb-item"><a href="#">Home</a></li>
                <li className="breadcrumb-item "><a href="#">Avaliações</a></li>
                <li className="breadcrumb-item active" aria-current="page">Nova avaliação</li>
              </ol>
            </nav>
            <p className="progress-title">Seu progresso:</p>
            <div className="progress" style={{ height: '20px' }}>
              <div className="progress-bar" role="progressbar" style={{ width: '0%' }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div className="evaluation-wrapper row ">
              <div className="evaluation-section col-12 col-md-6 col-sm-12">
                <div className="mb-3">
                  <div className="d-flex">
                    <h3>Qual software vamos avaliar?</h3>
                  </div>
                  <label className="sr-only" htmlFor="nomeSoftware">Digite o nome do software...</label>
                  <input
                    type="text"
                    className="form-control medium"
                    name="nomeSoftware"
                    placeholder="Digite o nome do software..."
                    onChange={(e) => this.setState({ data: { ...data, name: e.target.value } })}
                  >

                  </input>
                </div>
                <div>
                  <div className="d-flex justify-content-between">
                    <h3>O que vamos avaliar?</h3>
                    <button type="button"  data-tip="hello world" data-place="left" className="btn btn-white  bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button>
                  </div>
                  <div className="btn-group btn-group-toggle d-flex" data-toggle="buttons">
                    <label className="btn btn-white border d-flex flex-column justify-content-between align-items-center" onClick={() => this.setCategory(3)}>
                      <FontAwesomeIcon icon={faAccessibleIcon} size="3x" className="mb-3 mt-3" />
                      <input type="checkbox" className="px-2" autoComplete="off" />Acessibilidade
                    </label>
                    <label className="btn btn-white border d-flex flex-column justify-content-between align-items-center" onClick={() => this.setCategory(1)}>
                      <FontAwesomeIcon icon={faChalkboardTeacher} size="3x" className="mb-3 mt-3" />
                      <input type="checkbox" className="px-2" autoComplete="off" />Aprendizagem
                    </label>
                    <label className="btn btn-white border d-flex flex-column justify-content-between align-items-center" onClick={() => this.setCategory(2)}>
                      <img src={Usability} alt="" className="mb-3 mt-3" style={{ height: '50px' }} />
                      <input type="checkbox" className="px-2" autoComplete="off" />Usabilidade
                    </label>
                  </div>
                </div>
              </div>
              <div className="evaluation-section col-12 col-md-6 mb-3 col-sm-12 ">
                <div className="d-flex justify-content-between">
                  <h3>Funciona em qual plataforma?</h3>
                  <button type="button"  data-tip="hello world" data-place="left" className="btn btn-white  bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button>
                </div>
                <div className="btn-group btn-group-toggle d-flex" data-toggle="buttons">
                  <label className="btn btn-white border d-flex flex-column justify-content-between align-items-center" onClick={() => this.setPlatform('Web')}>
                    <FontAwesomeIcon icon={faGlobe} size="3x" className="mb-3 mt-3" />
                    <input type="radio" name="options" className="px-2" autoComplete="off" /> Web
                </label>
                  <label className="btn btn-white border d-flex flex-column justify-content-between align-items-center" onClick={() => this.setPlatform('Mobile')}>
                    <FontAwesomeIcon icon={faMobileAlt} size="3x" className="mb-3 mt-3" />
                    <input type="radio" name="options" className="px-2" autoComplete="off" /> Mobile
                </label>
                  <label className="btn btn-white border d-flex flex-column justify-content-between align-items-center" onClick={() => this.setPlatform('Desktop')}>
                    <FontAwesomeIcon icon={faDesktop} size="3x" className="mb-3 mt-3" />
                    <input type="radio" name="options" className="px-2" autoComplete="off" /> Desktop
                </label>
                </div>
              </div>
            </div>
            <button type="button" className="btn btn-light confirm border btn-green" onClick={this.createEvaluation}>Continuar</button>
          </div>
        </main>
        <ReactTooltip />
      </div>
    )
  }
}

export default NewEvaluation;
