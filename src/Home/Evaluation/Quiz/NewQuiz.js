import React, { Component } from 'react'
import './NewQuiz.sass';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faQuestionCircle,
  faTired,
  faFrownOpen,
  faMeh,
  faGrin,
  faGrinStars,
} from '@fortawesome/free-regular-svg-icons';
import {
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Header from '../../../Header/Header';

class NewQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionState: []
    };
  }

  componentDidMount() {
    this.setState({
      questionState: []
    });
  }

  toggleQuestionOptions(event, question) {
    let states = this.state.questionState;

    states[question] = !states[question];

    this.setState({
      questionState: states
    });
  }

  render() {
    return (
      <div className="container">
        <Header />
        <main>
          <div className="container">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb" style={{ backgroundColor: 'rgba(233,236,239, 0.5)' }}>
                <li className="breadcrumb-item"><a href="#">Home</a></li>
                <li className="breadcrumb-item "><a href="#">Avaliações</a></li>
                <li className="breadcrumb-item active" aria-current="page">Nova avaliação</li>
              </ol>
            </nav>
            <p className="progress-title">Seu progresso:</p>
            <div className="progress" style={{ height: '20px' }}>
              <div className="progress-bar progress-bar-green" role="progressbar" style={{ width: '5%' }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              <div className="progress-bar progress-bar-grey" role="progressbar" style={{ width: '66.6%' }} aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div className="evaluation-wrapper row ">
              <div className="evaluation-section col-12 col-md-4 mb-3 col-sm-12">
                <div className="d-flex justify-content-between align-items-center mb-2">
                  <h3>Aprendizagem</h3>
                  <button type="button" data-tip="Aprendizagem ..." data-place="left" className="btn btn-white  bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button>
                </div>
                <ul className="list-group">
                  <li className="list-group-item d-flex justify-content-between align-items-center active">
                    Papel do Aluno
                    <span className="badge badge-secondary badge-pill">14</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Incentivo ao Aluno
                    <span className="badge badge-secondary badge-pill">2</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Feedbacks e Resultados
                    <span className="badge badge-secondary badge-pill">4</span>
                  </li>
                </ul>
                <div className="d-flex justify-content-between align-items-center mb-2 mt-3">
                  <h3>Usabilidade</h3>
                  <button type="button" data-tip="Usabilidade ..." data-place="left" className="btn btn-white  bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button>
                </div>
                <ul className="list-group">
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Papel do Aluno
                    <span className="badge badge-secondary badge-pill">14</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Incentivo ao Aluno
                    <span className="badge badge-secondary badge-pill">2</span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Feedbacks e Resultados
                    <span className="badge badge-secondary badge-pill">4</span>
                  </li>
                </ul>
              </div>
              <div className="evaluation-section col-12 col-md-8 mb-3 col-sm-12 ">
                <h4 className="mb-3">O papel do aluno:</h4>
                <div className="card bg-light border-0 mb-3">
                  <div className="card-body">
                    <div className="d-flex justify-content-between">
                      <h3 className="card-title">1. O software motiva o aluno a executar as tarefas?</h3>
                      <button type="button" data-tip="Conteudo explicativo" data-place="left" className="btn btn-white  bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button>
                    </div>
                    <div className=" btn-group btn-group-toggle d-flex" data-toggle="buttons">
                      <label className="btn btn-white border-right btn-red" data-tip="Não aplica o item de nenhuma forma" data-place="bottom">
                        <FontAwesomeIcon icon={faTired} size="3x" className="mb-3 mt-3" />
                        <input type="radio" name="options1" autoComplete="off" disabled={this.state.questionState[0] !== undefined && !(this.state.questionState[0] === false)} />
                      </label>
                      <label className="btn btn-white border-right btn-orange " data-tip="De forma quase imperceptível, com uma implementação inicial ou quase inesistente " data-place="bottom">
                        <FontAwesomeIcon icon={faFrownOpen} size="3x" className="mb-3 mt-3" />
                        <input type="radio" name="options2" autoComplete="off" disabled={this.state.questionState[0] !== undefined && !(this.state.questionState[0] === false)} />
                      </label>
                      <label className="btn btn-white border-right btn-gray" data-tip="De forma mediana, como uma implementação funcional" data-place="bottom">
                        <FontAwesomeIcon icon={faMeh} size="3x" className="mb-3 mt-3" />
                        <input type="radio" name="options3" autoComplete="off" disabled={this.state.questionState[0] !== undefined && !(this.state.questionState[0] === false)} />
                      </label>
                      <label className="btn btn-white border-right btn-blue" data-tip="De forma usual, mas precisa de melhorias" data-place="bottom">
                        <FontAwesomeIcon icon={faGrin} size="3x" className="mb-3 mt-3" />
                        <input type="radio" name="options4" autoComplete="off" disabled={this.state.questionState[0] !== undefined && !(this.state.questionState[0] === false)} />
                      </label>
                      <label className="btn btn-white border-left btn-green" data-tip="De forma completa e usual" data-place="bottom">
                        <FontAwesomeIcon icon={faGrinStars} size="3x" className="mb-3 mt-3" />
                        <input type="radio" name="options5" autoComplete="off" />
                      </label>
                    </div>
                    <div className="text-right mt-3 btn-group-toggle" data-toggle="buttons">
                      <label className="btn bg-transparent btn-red btn-sm border" onClick={(e) => this.toggleQuestionOptions(e, 0)}>
                        <input type="checkbox" autoComplete="off" />
                        <FontAwesomeIcon icon={faTimes} className="mr-1" />
                        Não consegui avaliar isto
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Link to="/laudo">
              <button type="button" className="btn btn-light confirm border btn-green">Continuar</button>
            </Link>
          </div>
        </main>
        <ReactTooltip />
      </div>

    )
  }
}

export default NewQuiz;
