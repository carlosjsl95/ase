import React, { Component } from 'react'
import './ReportPage.sass';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import ReactTooltip from 'react-tooltip';
import Header from '../../../../Header/Header';

class ReportPage extends Component {
    render() {
        return (
            <div className="container">
                <Header />
                <main>
                    <div className="container">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb" style={{ backgroundColor: 'rgba(233,236,239, 0.5)' }}>
                                <li className="breadcrumb-item"><a href="">Home</a></li>
                                <li className="breadcrumb-item "><a href="#">Avaliações</a></li>
                                <li className="breadcrumb-item "><a href="#">Nova avaliação</a></li>
                                <li className="breadcrumb-item active" aria-current="page">Laudo</li>
                            </ol>
                        </nav>
                        <p className="progress-title">Seu progresso:</p>
                        <div className="progress" style={{ height: '20px' }}>
                            <div className="progress-bar progress-bar-green" role="progressbar" style={{ width: '100%' }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div className="evaluation-wrapper row ">
                            <div className="evaluation-section col-12 col-md-4 mb-3 col-sm-12">
                                <div className="d-flex justify-content-center align-items-center mb-2">
                                    <h3>Acessibiliadade</h3>
                                    {/* <button className="btn bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button> */}
                                </div>
                                <div className="d-flex justify-content-center mb-3">
                                    <CircularProgressbar className="circular-bar" value={50} text={`${50}%`} />
                                </div>
                                <div>
                                    <p className="text-justify">
                                        In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
                                    Lorem ipsum may be used before final copy is available, but it may also be used to temporarily replace copy in a process called greeking, which allows designers to consider form without the meaning of the text influencing the design.
                                    </p>
                                </div>
                            </div>
                            <div className="evaluation-section col-12 col-md-4 mb-3 col-sm-12">
                                <div className="d-flex justify-content-center align-items-center mb-2">
                                    <h3>Usabilidade</h3>
                                    {/* <button className="btn bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button> */}
                                </div>
                                <div className="d-flex justify-content-center mb-3">
                                    <CircularProgressbar className="circular-bar" value={50} text={`${50}%`} />
                                </div>
                                <div>
                                    <p className="text-justify">
                                        In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
                                    Lorem ipsum may be used before final copy is available, but it may also be used to temporarily replace copy in a process called greeking, which allows designers to consider form without the meaning of the text influencing the design.
                                    </p>
                                </div>
                            </div>
                            <div className="evaluation-section col-12 col-md-4 mb-3 col-sm-12">
                                <div className="d-flex justify-content-center align-items-center mb-2">
                                    <h3>Aprendizagem</h3>
                                    {/* <button className="btn bg-transparent"><FontAwesomeIcon icon={faQuestionCircle} size="lg" /></button> */}
                                </div>
                                <div className="d-flex justify-content-center mb-3">
                                    <CircularProgressbar className="circular-bar" value={50} text={`${50}%`} />
                                </div>
                                <div>
                                    <p className="text-justify">
                                        In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
                                    Lorem ipsum may be used before final copy is available, but it may also be used to temporarily replace copy in a process called greeking, which allows designers to consider form without the meaning of the text influencing the design.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="evaluation-wrapper row" >
                            <div className="evaluation-section col-12 d-flex justify-content-center    align-items-center mb-2">
                                <h3 className="text-primary">RESULTADO GERAL DO SOFTWARE</h3>
                            </div>
                            <div className="evaluation-section col-12 d-flex justify-content-center    align-items-center mb-2">
                                <p className="text-justify">
                                    Imperdiet dui accumsan sit amet nulla. Et malesuada fames ac turpis egestas.
                                    Id venenatis a condimentum vitae sapien pellentesque habitant morbi.
                                    Facilisis mauris sit amet massa vitae. Amet consectetur adipiscing elit pellentesque habitant.
                                    Suspendisse sed nisi lacus sed viverra. Nullam non nisi est sit amet. Fermentum leo vel orci porta.
                                    Odio euismod lacinia at quis risus. Convallis tellus id interdum velit.
                                    Molestie a iaculis at erat pellentesque adipiscing commodo elit. Consectetur a erat nam at lectus.
                                    Quis vel eros donec ac odio tempor orci dapibus ultrices. Fringilla est ullamcorper eget nulla facilisi.
                                    Facilisis volutpat est velit egestas dui id. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare.
                            </p>
                            </div>
                        </div>

                        <button type="button" className="btn btn-light confirm border btn-green">Finalizar</button>
                    </div>
                </main>
            </div>

        )
    }
}

export default ReportPage;
