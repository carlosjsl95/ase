import React, { Component } from 'react'
import './AboutPage.sass';
import { Link } from 'react-router-dom';
import ExternalHeader from '../../ExternalHeader/ExternalHeader';

class AboutPage extends Component {
  render() {
    return (
      <div className="container">
        <ExternalHeader />
        <main>
          <div className="container main bg-white row">
            <div className="col">
              <h3 id="sobre"> Sobre o EvaEduc</h3>
              <p class="text-justify" >
                O EvaEduc é uma ferramenta de avaliação de software criada por alunos do curso de Bacharelado
               em Tecnologia da informação, do Instituro Metrópole Digital (IMD), da Universidade Federal
               do Rio Grande do Norte(UFRN), sob orientação da Professora Adja Ferreira de Andrade.
               </p>
              <p className="text-justify">
                A ferramenta vai permitir a análise de software da web, desktop e mobile. As questões foram feitas
                com base em métodos de avaliação já reconhecidos. Sendo algumas questões criadas por nossa equipe
                visando atender a critérios pedagógicos usabilidade ergonômica e acessibilidade.
              </p>
              <p class="text-justify">
                Se necessita de informações sobre como usar nossa ferramenta, você pode <Link to="">conferir o manual feito </Link>
                por nossa equipe.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
            <h3 id="equipe"> Equipe do EvaEduc</h3>
            </div>
          </div>
        </main>
      </div>
    )
  }
}

export default AboutPage;
