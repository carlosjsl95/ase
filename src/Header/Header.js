import React, { Component } from 'react';
import './Header.sass';
import Logo from '../assets/logo.svg';
import AccessibiltyOptions from '../AccessibiltyOptions/AccessibiltyOptions';

class Header extends Component {

  state = {
    user: null
  }

  componentDidMount() {
    this.getUser()
  }

  getUser = () => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    this.setState({
      user
    });
  }

    render() {
      const { user } = this.state;
      return (
        <header className="header">
          <nav className="navbar navbar-light bg-white justify-content-between align-items-end">
            <a className="navbar-brand" href="/">
              <img className="logo" src={Logo} alt="EvaEduc"/>
            </a>
            <div>
              <AccessibiltyOptions/>
              <div className="btn-group">
                <button type="button" className="btn btn-primary dropdown-toggle btn-evaluation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Avaliações
                </button>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="#">Action</a>
                  <a className="dropdown-item" href="#">Another action</a>
                  <a className="dropdown-item" href="#">Something else here</a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">Separated link</a>
                </div>
              </div>
              <div className="btn-group">
                <button type="button" className="btn bg-white dropdown-toggle btn-username border" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {user && user.name.split(' ')[0]}
                </button>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="#">Perfil</a>
                  <a className="dropdown-item" href="#">Another action</a>
                  <a className="dropdown-item" href="#">Something else here</a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#">Separated link</a>
                </div>
              </div>
            </div>
          </nav>
        </header>
      )
    }
}

export default Header;