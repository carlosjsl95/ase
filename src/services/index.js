import axios from '../config/axios';

export const login = (data) => axios.post('/authenticate', data);

export const register = (data) => axios.post('/register', data);

export const newEvaluation = (data) => axios.post('/evaluations', data);

export const setCredentials = (token) => axios.defaults.headers.common['Authorization'] = `Bearer ${token}`