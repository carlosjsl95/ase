import React from 'react'

import {
  HashRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'
import Login from './Login/Login';
import Home from './Home/Home';
import NewEvaluation from './Home/Evaluation/NewEvaluation';
import NewQuiz from './Home/Evaluation/Quiz/NewQuiz';
import ReportPage from './Home/Evaluation/Quiz/Report/ReportPage';
import Register from './Register/Register';
import AboutPage from './Home/About/AboutPage';

export const Routes = (props) => {
  return (
    <Router>
      <Switch>
        {/* <PrivateRoute
          path="/home"
          component={Home}
          authenticated={props.auth.allowed}
        /> */}
        <Route
          exact
          path="/"
          component={Home}
        />
        <Route
          path="/login"
          component={Login}
        />
        <Route
          path="/cadastrar"
          component={Register}
        />
        <Route
          path="/avaliacao"
          component={NewEvaluation}
        />
        <Route
          path="/questionario"
          component={NewQuiz}
        />
        <Route
          path="/laudo"
          component={ReportPage}
        />
        <Route
          path="/sobre"
          component={AboutPage}
        />
      </Switch>
    </Router>
  )
}